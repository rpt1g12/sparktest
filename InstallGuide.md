# Guía de instalación para las pruebas de Spark en Java

## Obtención de los instaladores, ejecutables y scripts

Hemos proporcionado las herramientas necesarias para realizar las pruebas en un archivo comprimido
que se puede descargar en:

  * [SparkJava](https://bitbucket.org/rpt1g12/sparktest/src/master/spark.7z)

Una vez descargado, descomprime el archivo y encotrarás:
  * Un instalador de Java8 (jdk-8u281-windows-x64.exe)
  * Un instalador del IDE IntelliJ-IDEA (ideaIC-2020.3.2.exe)
  * Un archivo con las librerias de Spark2.4.7
  * Un ejecutable que permite usar hadoop en el entorno Windows
  * Un script de PowerShell que hace el setup de las variables de entorno
