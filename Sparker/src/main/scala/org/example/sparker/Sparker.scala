package org.example.sparker

import org.apache.spark.sql.SparkSession

object Sparker {

  lazy val jarPath = this.getClass.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()

  def initSparkSession():SparkSession = {
    SparkSession.builder()
      .master("local[*]")
      .appName("spark-console")
      .config("jars",jarPath)
      .getOrCreate()
  }

}
