import org.apache.spark.sql.types.{StructField, StructType}
import org.example.sparker.Sparker

val sparkSession = Sparker.initSparkSession()

import sparkSession.implicits._
val df = Seq(
  ("a", 1),
  ("b", 2)
).toDF("s","int")

val st = new StructType()
StructField.w